
const St = imports.gi.St;
const Main = imports.ui.main;
const Tweener = imports.ui.tweener;
const Util = imports.misc.util;
const GLib = imports.gi.GLib;

function init() {
        //Nothing here to not mess with the system unless enabled
}

var action_button;

function enable() {
        let system_menu = Main.panel.statusArea.aggregateMenu._system;

        action_button = system_menu._createActionButton("night-light-symbolic", _("Toggle Dark Theme"));
        action_button.connect('clicked', () => {
                let current_theme = GLib.spawn_command_line_sync("gsettings get org.gnome.desktop.interface gtk-theme")[1].toString();
                if (/dark/i.test(current_theme)) {
                        GLib.spawn_command_line_async("gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita'");
                } else {
                        GLib.spawn_command_line_async("gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita-dark'");
                }
        });

        system_menu._actionsItem.actor.add(action_button, {expand: true, x_fill: false});
}

function disable() {
        action_button.destroy();
}
