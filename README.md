# Dark mode toggler for Gnome
Adds a button to the aggregated menu of Gnome shell that toggles dark mode. Currently only works for Adwaita, however, support for all themes will be added. Only tested on the latest Gnome 3.28.2.

![demo](https://bytebucket.org/lukajankovic/gnome-dark-mode-toggle/raw/86dc178e6ca120062988b41e8be97733b816dcff/demo.gif)

## Installation
Download and place in ```~/.local/share/gnome-shell/extensions/darkmodetoggle@am.alite ```